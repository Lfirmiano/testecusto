<style>
    table {
        margin-top: 2%;
    }
</style>

<x-default title="Listagem de Opcoes">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Company</th>
                <th scope="col">Price</th>
            </tr>
        </thead>
        <tbody>
            @php($count=0)
            @foreach ($bestOptionsArray as $company => $price)
                <tr>
                    <td>
                        {{ $count }}
                    </td>
                    <td>
                        {{ $company }}
                    </td>
                    <td>
                        {{ number_format($price, 2) }}
                    </td>
                </tr>
                @php($count++)
            @endforeach
        </tbody>
    </table>

</x-default>