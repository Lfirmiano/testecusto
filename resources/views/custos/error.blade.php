<x-default title="Erro">
    <h3>
        A requisicao nao pode ser concluida. Motivo: <br><br> 
        <span style="color: red;">
            {{ $motivo }}
        </span>
    </h3>
</x-default>