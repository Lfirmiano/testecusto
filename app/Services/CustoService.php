<?php 

namespace App\Services;

class CustoService {
    private $baseUrl;

    public function __construct()
    {
        $this->baseUrl = "https://jason.dev.br/";
    }

    public function doRequest(string $baseUrl, string $method = 'GET') {
        $curl = curl_init();

        $headers = [
            "Accept:application/json"
        ];

        curl_setopt_array($curl, [
            CURLOPT_URL            => $baseUrl,
            CURLOPT_HTTPHEADER     => $headers,
            CURLOPT_CUSTOMREQUEST  => $method,
            CURLOPT_RETURNTRANSFER => true,         // return web page
            CURLOPT_AUTOREFERER    => true,         // set referer on redirect
            CURLOPT_TIMEOUT        => 120,          // timeout on response
            CURLOPT_MAXREDIRS      => 10,           // stop after 10 redirects
            CURLOPT_VERBOSE        => 1          
        ]);

        $response       = curl_exec($curl);
        $error          = curl_error($curl);
        $responseStatus = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return [json_decode($response), $error, $responseStatus];
    }

    public function getAllOptions() {
        $customUrl = "{$this->baseUrl}/testecusto";
        return $this->doRequest($customUrl);
    }
}