<?php

namespace App\Http\Controllers;

use App\Services\CustoService;
use Illuminate\Http\Request;

class CustoController extends Controller
{
    public function bestOption($quantity) {
        $custoService     = new CustoService();
        $callback         = $custoService->getAllOptions();

        // ERROR TREATMENT
        if (!empty($callback[1]) || $callback[2] != 200) return view('custos.error', ['motivo' => $callback[1]]);

        $bestOptionsArray = $this->getBestOptions($callback[0], $quantity);
        $bestOptionsArray = $this->sortOptions($bestOptionsArray);

        return view('custos.bestOption', compact('bestOptionsArray'));
    }

    // GET THE RESULT FROM REQUEST AND SET THE BEST OPTIONS
    private function getBestOptions($results, $quantity) {
        $bestOptions = [];
        foreach ($results->data as $result) {
            foreach ($result->price as $key => $price) {
                if ($quantity <= $key) {
                    $bestOptions[$result->label] = $price;
                    break;
                } else {
                    $bestOptions[$result->label] = $price;
                }
            }
        }
        return $bestOptions;
    }

    // SORT THE ARRAY TO ORDER FROM CHEAPER TO MORE EXPENSIVE
    private function sortOptions($options) {
        $pricesArray    = [];
        $companiesArray = [];
        $sortedOptions  = [];

        foreach ($options as $company => $price) {
            $companiesArray[] = $company;
            $pricesArray[]    = $price;
        };

        $priceArraySize = sizeof($pricesArray);
        for($i = 0; $i < $priceArraySize; $i++){
            for ($j = 0; $j < $priceArraySize - $i - 1; $j++){
                if ($pricesArray[$j] > $pricesArray[$j+1]){
                    // ORDER PRICE ARRAY
                    $auxPrice          = $pricesArray[$j];
                    $pricesArray[$j]   = $pricesArray[$j+1];
                    $pricesArray[$j+1] = $auxPrice;

                    // ORDER COMPANIES ARRAY
                    $auxCompany           = $companiesArray[$j];
                    $companiesArray[$j]   = $companiesArray[$j+1];
                    $companiesArray[$j+1] = $auxCompany;
                }
            }
        }

        foreach ($pricesArray as $key => $price) {
            $sortedOptions[$companiesArray[$key]] = $price;
        }

        return $sortedOptions;
    }
}
